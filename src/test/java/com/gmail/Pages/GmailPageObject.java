package com.gmail.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class GmailPageObject {

    @FindBy(id = "identifierId")
    private WebElement loginInput;

    @FindBy(xpath = "//div[@id='identifierNext']")
    private WebElement nextButton;

    @FindBy(xpath = "//div//input[@type='password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//div[@id='passwordNext']")
    private WebElement nextPassButton;

    @FindBy(xpath = "//div[@class='oZ-jc T-Jo J-J5-Ji ' and @role='checkbox']")
    private List<WebElement> selectMessage;

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji mA nf T-I-ax7 L3' and @role='button']//span[@class='asa bjy']")
    private WebElement optionsButton;

    @FindBy(xpath = "//div[@class='J-M aX0 aYO jQjAxd' and @role='menu']//div[@class='SK AX']//div[4]//div[1]")
    private WebElement importantBtn;

    @FindBy(xpath = "span[@class='ait'//div[@class='G-asx T-I-J3 J-J5-Ji']")
    private WebElement moreBtn;

    @FindBy(xpath = "//span[@class='nU ']")
    private WebElement importantFolder;

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji nX T-I-ax7 T-I-Js-Gs mA' and @role='button']//div[@class='asa']")
    private WebElement deleteBtn;

    @FindBy(xpath = "//div[@class='vh']//span[@class='aT']")
    private WebElement importantAlert;

    public GmailPageObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void typeLogin(String login) {
        loginInput.sendKeys(login);
        nextButton.click();
    }

    public void typePassword(String password) {
        passwordInput.sendKeys(password);
        nextPassButton.click();
    }

    public void selectMessage() {
        for (int i = 0; i < 3; i++) {
            selectMessage.get(i).click();
        }
    }

    public void openMsgOptions() {
        optionsButton.click();
    }

    public void moveMsgToImportantFolder() {

        importantBtn.click();
    }

    public void verifyImportantAction() {
        if (importantAlert.isDisplayed()) {
            for (int i = 0; i < 3; i++) {
                selectMessage.get(i).click();
            }
        }
    }


    public void deleteMsg() {

        deleteBtn.click();
    }
}
