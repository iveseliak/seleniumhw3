package com.gmail.Test;

import com.gmail.Pages.GmailPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class GmailTest {
    WebDriver driver;

    @BeforeTest
    public void setURL(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.gmail.com");
    }

    @Test
    public void gmailTest(){
        GmailPageObject object=new GmailPageObject(driver);
        object.typeLogin("test.test");
        (new WebDriverWait(driver,30)).until(visibilityOfElementLocated(By.cssSelector("input[type='password']")));
        object.typePassword("qwerty1");
        (new WebDriverWait(driver,30)).until(visibilityOfElementLocated(By.xpath("//div[@class='oZ-jc T-Jo J-J5-Ji ' and @role='checkbox']")));
        object.selectMessage();
        object.openMsgOptions();
        (new WebDriverWait(driver,10)).until(visibilityOfElementLocated(By.xpath("//div[@class='J-M aX0 aYO jQjAxd' and @role='menu']//div[@class='SK AX']//div[4]//div[1]")));
        object.moveMsgToImportantFolder();


        object.deleteMsg();
    }

    @AfterTest
    public void quitDriver(){
        driver.quit();
    }

}
